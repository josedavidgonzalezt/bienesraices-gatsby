const urlSlug = require('url-slug')
exports.createPages = async ({ actions, graphql, reporter }) => {
  const resultado = await graphql(`
    query {
      allStrapiPaginas{
        nodes {
          nombre
          id
        }
      },
      allStrapiPropiedades {
        nodes {
          nombre
          id
        }
      }
    }
  `)

  if(resultado.erros) {
    reporter.panic('no hubo resultados', resultado.errors)
  }
  // propiedades
  resultado.data.allStrapiPropiedades.nodes.forEach( propiedad => {
    actions.createPage({
      path: urlSlug(propiedad.nombre),
      component: require.resolve(`./src/components/propiedad.js`),
      context: {
        slug: propiedad.id,
      },
    })
  })

  resultado.data.allStrapiPaginas.nodes.forEach( pagina => {
    actions.createPage({
      path: urlSlug(pagina.nombre),
      component: require.resolve(`./src/components/paginas.js`),
      context: {
        slug: pagina.id,
      },
    })
  })
}