import React from 'react'

import Layout from '../components/layout'
import useInicio from '../hooks/useinicio'
import { css } from '@emotion/react';
import styled from '@emotion/styled';
import BackgroundImage from 'gatsby-background-image'
import * as heroCSS from '../css/hero.module.css'
import Encuentra from '../components/encuentra';
import ListadoPropiedades from '../components/listadoPropiedades';

const ImagenBackgroud = styled(BackgroundImage)`
  height: 600px;

`

const IndexPage = () => {
  const inicio = useInicio()
  const { contenido, nombre, imagen } = inicio;
  const imagenes = imagen.childImageSharp.fluid
  
  return (
    <Layout>
      <ImagenBackgroud
        tag='section'
        fluid={imagenes}
        fadeIn={'soft'}
      >
        <div className={heroCSS.imagenbg}>
          <h1 className={heroCSS.titulo}>Venta de Casas y Departamentos Exclusivos</h1>
        </div>
      </ImagenBackgroud>
      <main>
        <div
          css={css`
            max-width: 800px;
            margin: 0 auto;
            white-space: break-spaces;
          `}
        >
        <h1>{nombre}</h1>
        <p
          css={css`
            text-align: center;
          `}
        >{contenido}</p>
        </div>
      </main>
      <Encuentra/>
      <ListadoPropiedades/>
    </Layout>
  )
}

export default IndexPage
