import { css } from "@emotion/react"
import { Link, useStaticQuery, graphql } from "gatsby"
import React from "react"
import Navegacion from "./navegacion"

const Header = () => {

  const { logo } = useStaticQuery(graphql`
  query{
  	logo: file (relativePath: {eq: "logo.svg"}) {
  		publicURL
      id
    }
  }
`)
  return (
    <header
      css={css`
        background-color: #0D283D;
        padding: 1rem;
      `}
    >
      <div
        css={css`
          max-width: 120rem;
          margin: 0 auto;
          text-align: center;
          @media (min-width: 778px) {
            display: flex;
            align-items: center;
            justify-content: space-between;
          }
        `}
      >
        <Link
          to={'/'}
        >
          <img
            src={logo.publicURL}
            alt='logo'
          />
        </Link>
        <Navegacion/>
      </div>
    </header>
  )
}

export default Header
