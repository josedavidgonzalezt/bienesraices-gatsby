import React from "react"
import styled from "@emotion/styled"
import Image from "gatsby-image"
import Iconos from "./iconos"
import Layout from "./layout"
import { graphql } from "gatsby"

const Contenido = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  width: 95%;

  @media (min-width: 768px){
    display: grid;
    grid-template-columns: 2fr 1fr;
    column-gap: 5rem;
  }
`
const Sidebar = styled.aside`
  .precio {
    font-size: 2rem; 
    color: #75AB00;
  }
  .agente {
    margin-top: 4rem;
    border-radius: 2rem;
    background-color: #75AB00;
    padding: 3rem;
    color: #FFF;
  }
  p {
    margin: 0;
  }
`

export const datos = graphql`
  query ($slug: String){
   allStrapiPropiedades (filter: {id : {eq: $slug }}) {
     nodes {
      nombre
      estacionamiento
      wc
      id
      descripcion
      habitaciones
      precio
      agentes {
        nombre
        telefono
        email
      }
      imagen {
        childImageSharp {
          fluid(maxWidth: 600, maxHeight: 400) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }

     }
   }
  }
`

const Propiedad = ({data}) => {
 const {nombre, estacionamiento, wc, descripcion, habitaciones, precio, agentes, imagen} = data.allStrapiPropiedades.nodes[0]
  return ( 
    <Layout>
      <h1>{nombre}</h1>
      <Contenido>
        <main>
          <Image 
            fluid={imagen.childImageSharp.fluid}
            fadeIn="soft"
          />
          <p className="precio">{descripcion}</p>
        </main>
        <Sidebar>
          <p>{precio}</p>
          <Iconos
            estacionamiento={estacionamiento}
            wc={wc}
            habitaciones={habitaciones}
          />
          <div className="agente">
            <h2>Vendedor:</h2>
            <p> {agentes.nombre}</p>
            <p>Tel: {agentes.telefono}</p>
            <p>Email: {agentes.email}</p>
          </div>
        </Sidebar>
      </Contenido>
    </Layout>
   );
}
 
export default Propiedad;