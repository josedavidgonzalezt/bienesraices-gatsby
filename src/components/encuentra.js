import React from 'react';
import styled from '@emotion/styled';
import { graphql, useStaticQuery } from 'gatsby';
import BackgroundImage from 'gatsby-background-image';
import * as heroCSS from '../css/hero.module.css'

const ImagenBackgroud = styled(BackgroundImage)`
  height: 300px;
`

const Encuentra = () => {

  const { imagen } = useStaticQuery(graphql`
    query{
      imagen:	file (relativePath: {eq: "encuentra.jpg"}){
    		childImageSharp {
          fluid(maxWidth: 1500) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
    }
  `)
  return ( 
    <ImagenBackgroud
      tag="section"
      fluid={imagen.childImageSharp.fluid}
      fadeIn="soft"
    >
      <div className={heroCSS.imagenbg}>
        <h3 className={heroCSS.titulo}>Encuentra la casa de tus sueños</h3>
        <p>15 años de experiencia</p>

      </div>
    </ImagenBackgroud> );
}
 
export default Encuentra;