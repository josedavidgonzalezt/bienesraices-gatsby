import React, { useEffect, useState } from 'react';
import { css } from '@emotion/react';
import usePropiedades from '../hooks/usePropiedades';
import PropiedadPreview from './propiedadPreview';
import * as listadoCSS from '../css/listadopropiedades.module.css'
import useFiltro from '../hooks/useFiltro';

const ListadoPropiedades = () => {
  const resultado = usePropiedades()
  const [filtradas, guardarFiltradas] = useState([])
  const [propiedades] = useState(resultado)

  const { categoria, FiltroUI } = useFiltro();

  useEffect(() => {
    if (categoria) {
      const filtro = propiedades.filter(f => f.categorias.nombre === categoria)
      guardarFiltradas(filtro)
    } else {  
      guardarFiltradas(propiedades)
    }
    // eslint-disable-next-line
  }, [resultado, categoria])

  return ( 
    <>
      <h2
        css={css`
          margin-top: 5rem;
        `}
      >nuestras propiedades</h2>
      {FiltroUI()}
      <ul className={listadoCSS.propiedades}>
        {filtradas.map((propiedad, i) =>(
          <PropiedadPreview
            key={i}
            propiedad={propiedad}
          />
        ))}
      </ul>
    </>
   );
}
 
export default ListadoPropiedades;