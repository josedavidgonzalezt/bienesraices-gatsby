import { graphql, useStaticQuery } from 'gatsby'

const useInicio = () => {
  const datos = useStaticQuery(graphql`
    query {
    	allStrapiPaginas(filter: {nombre: {eq: "Inicio"} }){
    		nodes{
          id
    			nombre
          contenido
        	imagen {
    				childImageSharp {
              fluid(maxWidth: 1500 duotone: {
                highlight: "#222222", shadow: "#192550", opacity: 30
              }) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
      }
    }
  `)
  return datos.allStrapiPaginas.nodes[0]
}
 
export default useInicio;