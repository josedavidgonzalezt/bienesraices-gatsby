import React, {useState} from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import styled from '@emotion/styled';

const Formulario = styled.form`
  width: 100%;
  display: flex;
  margin-top: 2rem;
  border: 1px solid #E1E1E1;
  max-width: 1200px;
  margin: 2rem auto 0 auto;
`

const Select = styled.select`
  flex: 1;
  padding: 1rem;
  appearance: none;
  border: none;
  font-family: 'Lato', sans-serif;
`

const useFiltro = () => {
  // state
  const [selected, setSelected] = useState('')

  // query

  const {allStrapiCategorias: {nodes: categorias}} = useStaticQuery(graphql`
    query{
      allStrapiCategorias {
        nodes {
          nombre
          id
        }
      }
    }
  `)

  const FiltroUI = () => (
    <Formulario>
      <Select
        onChange={e => setSelected(e.target.value)}
        value={selected}
      >
        <option value=""> ---Filtrar--- </option>
        {categorias.map((categoria) => (
          <option
            key={categoria.id}
            value={categoria.nombre}
          >{categoria.nombre}</option>
        ))}
      </Select>
    </Formulario>
  )

  return {
    categoria: selected,
    FiltroUI
  }
}
 
export default useFiltro;