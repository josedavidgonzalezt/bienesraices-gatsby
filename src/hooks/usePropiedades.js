import { graphql, useStaticQuery } from "gatsby";

const usePropiedades = () => {
   const datos = useStaticQuery(graphql`
    query {
      allStrapiPropiedades {
        nodes {
          nombre
          descripcion
          id
          wc
          precio
          habitaciones
          estacionamiento
          categorias {
            nombre
            id
          }
          agentes {
            nombre
            email
            telefono
          }
          imagen {
            childImageSharp {
              fluid(maxWidth: 600, maxHeight: 400) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
      }
    }

   `)

   return datos.allStrapiPropiedades.nodes
}
 
export default usePropiedades;